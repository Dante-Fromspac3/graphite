# Characters.
## Hero.
define j = Character("James", who_color="#6495ed")
define e = Character("Emily", who_color="#ff52d5")
define ce = Character("Celia", who_color="#3e2a54")
#define a = Character("Anna", who_color="#3e2a54")

## Villain.
define c = Character("Caleb", who_color="#ffc82a")
define m = Character("Marcus", who_color="#006400")